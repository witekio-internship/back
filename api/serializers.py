from rest_framework import serializers

from api.models import Employee


class EmployeeSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Employee
        fields = ['id','name','email','picture','inside','position','time_of_arrival','time_of_departure']
