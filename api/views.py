from datetime import date

from django.shortcuts import render

# Create your views here.
from django.utils import timezone
from rest_framework import viewsets, renderers
from rest_framework.decorators import action
from rest_framework.response import Response

from api.models import Employee
from api.serializers import EmployeeSerializer


class EmployeeViewSet(viewsets.ModelViewSet):
    queryset = Employee.objects.all()
    serializer_class = EmployeeSerializer

    @action(detail=False, renderer_classes=[renderers.JSONRenderer])
    def inside(self, request, *args, **kwargs):
        employees_inside = [employee for employee in self.queryset if employee.inside == True ]
        return Response(EmployeeSerializer(employees_inside, context={'request': request}, many=True).data)

    @action(detail=True, renderer_classes=[renderers.JSONRenderer])
    def sign_in(self, request, *args, **kwargs):
        employee = self.get_object()
        employee.last_visit_day = timezone.now().date()
        employee.time_of_arrival = timezone.now().time()
        employee.time_of_departure = None
        employee.save()
        return Response(EmployeeSerializer(employee, context={'request': request}).data)

    @action(detail=True, renderer_classes=[renderers.JSONRenderer])
    def sign_out(self, request, *args, **kwargs):
        employee = self.get_object()
        employee.time_of_departure = timezone.now().time()
        employee.save()
        return Response(EmployeeSerializer(employee, context={'request': request}).data)
