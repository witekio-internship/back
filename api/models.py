from django.db import models

# Create your models here.
from django.utils import timezone


class Person(models.Model):
    name = models.CharField(max_length=100)
    email = models.EmailField(blank=True)
    picture = models.ImageField(blank=True)
    car_registration = models.CharField(max_length=30, null=True, blank=True)
    time_of_arrival = models.TimeField(null=True, blank=True)
    time_of_departure = models.TimeField(null=True, blank=True)
    last_visit_day = models.DateField(null=True, blank=True)

    @property
    def inside(self):
        if not self.last_visit_day == timezone.now().date():
            return False
        if self.time_of_arrival:
            if not self.time_of_departure:
                return True
        return False

    class Meta:
        abstract = True


class Employee(Person):
    position = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Visitor(Person):
    company = models.CharField(max_length=80)

    def __str__(self):
        return self.name
