from django.contrib import admin

# Register your models here.
from api.models import Employee


@admin.register(Employee)
class EmployeeAdmin(admin.ModelAdmin):
    def inside(self,instance):
        return instance.inside
    inside.boolean = True

    list_display = ('name', 'inside', 'last_visit_day', 'time_of_arrival', 'time_of_departure')
